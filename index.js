const { ApolloServer, gql } = require("apollo-server");
const axios = require("axios");

// A schema is a collection of type definitions (hence "typeDefs")
// that together define the "shape" of queries that are executed against
// your data.
const typeDefs = gql`
  # Comments in GraphQL strings (such as this one) start with the hash (#) symbol.

  type Weather {
    temp_c: Float
    temp_f: Float
    description: String
    humidity: Float
    wind_dir: String
    city: String
  }

  # The "Query" type is special: it lists all of the available queries that
  # clients can execute, along with the return type for each.
  type Query {
    weatherByLocation(city: String!): Weather
  }
`;

const API_KEY = "9e121a1ae2884bdbb3794911210906";

const weatherByLocation = async (city) => {
  const { data } = await axios.get(
    `http://api.weatherapi.com/v1/current.json?key=${API_KEY}&q=London&aqi=no`
  );
  const current = data.current;
  return {
    temp_c: current.temp_c,
    temp_f: current.temp_f,
    description: current.condition.text,
    humidity: current.humidity,
    wind_dir: current.wind_dir,
    city,
  };
};

// Resolvers define the technique for fetching the types defined in the
// schema. This resolver retrieves weather from the weather object above.
const resolvers = {
  Query: {
    weatherByLocation: (parent, { city }) => weatherByLocation(city),
  },
};

// The ApolloServer constructor requires two parameters: your schema
// definition and your set of resolvers.
const server = new ApolloServer({ typeDefs, resolvers });

// The `listen` method launches a web server.
server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});
