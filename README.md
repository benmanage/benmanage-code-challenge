# Benmanage Code Challenge - GraphQL weather API

## Introduction

The purpose of this exercise is for you to demonstrate how you approach solving problems outside your current range of skills.
It is important to commit incrementally, to showcase your work.

Feel free to include any comments on things you would do differently if you had more time, things you found more challenging, etc.

You can spend up to 1.5 hours on this challenge. Good luck!

## Instructions to get started

Clone the repository, open in your code editor, open a terminal, and install the npm packages with `npm i`

Start the apollo server with the command `npm start`

You can then open the apollo graphql playground in a browser: <http://localhost:4000/>

The index.js file includes a query to get current weather by location.

You can test this out in the playground by pasting in the following query:

```
query getWeatherByLocation {
  weatherByLocation(city: "Paris")  {
    temp_c,
    humidity,
    description,
    city
  }
}
```

WeatherAPI documentation: https://www.weatherapi.com/docs/

Apollo Server docs: <https://www.apollographql.com/docs/apollo-server/>

## Challenge #1

Expand the existing current weather query to allow users to optionally return the weather history from one year ago (from day of request).

See documentation here for api details: <https://github.com/weatherapicom/weatherapi-Node-js#-gethistoryweather>

The query would look something like this:

```
query getWeatherByLocation {
  weatherByLocation(city: "London")  {
    temp_c,
    humidity,
    description,
    last_year {
      temp_c,
      humidity
    }
  }
}
```

## Challenge #2

Create a new query that would allow a user to pass in a search string, eg 'Spring', and get back an array of matching locations with location name, sunrise and sunset.

The user expects the response to look something like this:

```
{
  "data": {
    "locations": [
      {
        "name": "Spring Valley",
        "sunrise": "05:48 AM",
        "sunset": "09:53 PM"
      },
      {
        "name": "Mountain Springs",
        "sunrise": "06:48 AM",
        "sunset": "08:53 PM"
      }
    ]
  }
}
```

Tip: use both the search/autocomplete and the astronomy apis

## Bonus

This part is optional.

The weather api allows you to check alerst as part of the forecast api. How can it be used to allow users to get notified via email/sms of upcoming weather alerts in their area?

Don't write any code for this question, instead, write up how you would plan and implement this feature, a brainstorming session of sorts.

Feel free to reach out with any questions!